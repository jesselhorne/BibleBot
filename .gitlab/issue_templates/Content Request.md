## What would you like to see implemented in BibleBot?
<!-- This is not for translations or language requests. This is for requesting a catechism, confession, or historical document to be added. -->

<!-- By submitting this form, you agree to follow our Code of Conduct: https://gitlab.com/kerygmadigital/biblebot/BibleBot/-/blob/master/CODE_OF_CONDUCT.md -->