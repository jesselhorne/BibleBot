## Summary
<!-- Summarize the bug encountered concisely -->

## Steps to reproduce
<!-- How one can reproduce the issue - this is very important -->

## What is the current bug behavior?
<!-- What actually happens -->

## What is the expected correct behavior?
<!-- What you should see instead -->

## Relevant screenshots
<!-- A visual example of what happens -->

<!-- By submitting this form, you agree to follow our Code of Conduct: https://gitlab.com/kerygmadigital/biblebot/BibleBot/-/blob/master/CODE_OF_CONDUCT.md -->