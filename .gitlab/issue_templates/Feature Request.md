## What would you like to see implemented in BibleBot?
<!-- This is not for translations, language, or content requests. This is for requesting a feature/functionality to be added. -->

<!-- By submitting this form, you agree to follow our Code of Conduct: https://gitlab.com/kerygmadigital/biblebot/BibleBot/-/blob/master/CODE_OF_CONDUCT.md -->